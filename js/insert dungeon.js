$(function () {
    //loadPage("data/pokemon.html");

    $('#dungeonselect').on('change', function () {
        loadPage($(this).find(':selected').data('url'));
    });
});

function loadPage(url) {
    $("#page").load(url);
}