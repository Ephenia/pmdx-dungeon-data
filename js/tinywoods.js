$(document).ready(function () {
    var table = $('#tinywoods').DataTable({
        "ajax": "ajax/tinywoods.json",
        "searching": false,
        "ordering": true,
        "lengthChange": false,
        "bPaginate": false,
        "info": false,
        "autoWidth": false,
        "order": [1,'asc'],
        "columns": [
            {
                "className": 'details-control',
                "orderable": false,
                "data": "image",
                "render": function (data) {
                    return '<img src="' + data + '"/>';
                }
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": "name"
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": "quantity"
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": "floor"
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": "spark"
            }
        ],
    });

    // Add event listener for opening and closing details
    $('#tinywoods tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
});