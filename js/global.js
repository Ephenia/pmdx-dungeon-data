/* Formatting function for row details - modify as you need */
function format(d) {
    // `d` is the original data object for the row
    return '<table class="innerlegend">' +
        '<tr>' +
        '<td>Floors:</td>' +
        '<td>' + d.floors + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>Sparkling Tile Floors:</td>' +
        '<td>' + d.tiles + '</td>' +
        '</tr>' +
        '</table>';
}